import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;


@Named(value = "mBFlujo")
@Dependent
public class MBFlujo {

    
    public MBFlujo() {
    }
    
    public void registrarEmpleado() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("registro_empleado.xhtml");
    }
    
    public void registrarAlumno() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("registro_alumno.xhtml");
    }
    
    public void registrarProfesor() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("registro_profesor.xhtml");
    }
}
