import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import sisescolar.modelos.Persona;
import sisescolar.modelos.Profesor;
import sisescolar.servicio.ProfesorService;
import sisescolar.servicio.ServicePersona;

@ManagedBean(name="mBPersona")
@ViewScoped
public class MBPersona implements Serializable{
    
    @EJB
    private ServicePersona servicePersona;
    
    @EJB
    private ProfesorService serviceProfesor;
    
    private int alumno = 0;
    
    private Persona persona;
    private Profesor profesor;
    
    private int id;
    private String passw;

    private List<Persona> personas;
    
        
    @PostConstruct
    public void inicializar(){
        persona = new Persona();
        profesor = new Profesor();
        this.personas = servicePersona.listarPersonas();
    }

    public MBPersona() {
        
    }
    
    public void volver() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("registro.xhtml");
    }
    
    
    public void insertarPersona() throws IOException{
    servicePersona.insertarPersona(this.persona);
    
    switch(this.persona.getRol()){
    
        case "Alumno": 
                                    
                                    break;
        
        case "Profesor": 
                                    serviceProfesor.insertarProfesor(this.profesor);
                                    FacesContext.getCurrentInstance().getExternalContext().redirect("menu_admin.xhtml");
                                    break;
                                    
        case "Empleado":
                                    
                                    break;
        default:     
                                    
                                    break;
    }
    
    }
    
    public void validarLogin() throws IOException{
    if(servicePersona.validarLogin(this.id, this.passw)){
        FacesContext.getCurrentInstance().getExternalContext().redirect("bienvenida.xhtml");
    } else{
        FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
    }
    
    }
    
    public List<Persona> getPersonas() {
        this.personas = servicePersona.listarPersonas();
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    public ServicePersona getServicePersona() {
        return servicePersona;
    }

    public void setServicePersona(ServicePersona servicePersona) {
        this.servicePersona = servicePersona;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
     public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAlumno() {
        return alumno;
    }

    public void setAlumno(int alumno) {
        this.alumno = alumno;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }
   
    
    
}
