package sisescolar.modelos;


public class Persona {
    
    private int idPersona;
    private String nombre;
    private String paterno;
    private String materno;
    private String sexo;
    private String telefono;
    private String email;
    private String passw;
    private String rol;

    public Persona(int idPersona, String nombre, String paterno, String materno, String sexo, String telefono, String email, String passw, String rol) {
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.sexo = sexo;
        this.telefono = telefono;
        this.email = email;
        this.passw = passw;
        this.rol = rol;
    }

    

    public Persona() {
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String aPaterno) {
        this.paterno = aPaterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String aMaterno) {
        this.materno = aMaterno;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    
    
}
