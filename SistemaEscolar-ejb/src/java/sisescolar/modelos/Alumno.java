package sisescolar.modelos;

public class Alumno {

    private int idPersona;
    private String clave;

    public Alumno(int idPersona, String clave) {
        this.idPersona = idPersona;
        this.clave = clave;
    }

    public Alumno() {
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    
    
}
