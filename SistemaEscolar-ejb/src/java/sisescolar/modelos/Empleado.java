package sisescolar.modelos;

public class Empleado {
    
    private int idEmpleado;
    private int idPersona;
    private String usuario;
    private String tipo;
    private String estado;
    private String fecha_acceso;

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFecha_acceso() {
        return fecha_acceso;
    }

    public void setFecha_acceso(String fecha_acceso) {
        this.fecha_acceso = fecha_acceso;
    }
    
    
    
}
