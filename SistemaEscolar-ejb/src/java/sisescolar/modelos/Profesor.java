package sisescolar.modelos;

public class Profesor {
    
    
    private int idPersona;
    private String cedula;
    private String grado_academico;
    private String especialidad; 
    
    private Persona persona;

    public Profesor(int idPersona, String cedula, String grado_academico, String especialidad, Persona persona) {
        this.idPersona = idPersona;
        this.cedula = cedula;
        this.grado_academico = grado_academico;
        this.especialidad = especialidad;
        this.persona = persona;
    }

    public Profesor() {
    }
    
    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getGrado_academico() {
        return grado_academico;
    }

    public void setGrado_academico(String grado_academico) {
        this.grado_academico = grado_academico;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    
    
}
