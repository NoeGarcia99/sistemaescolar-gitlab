package sisescolar.servicio;

import javax.ejb.Local;
import sisescolar.modelos.Profesor;

@Local
public interface ProfesorService {
    public void insertarProfesor(Profesor profesor);
}
