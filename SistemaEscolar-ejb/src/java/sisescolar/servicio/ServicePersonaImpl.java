package sisescolar.servicio;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import sisescolar.datos.PersonaDao;
import sisescolar.modelos.Persona;

@Stateless
public class ServicePersonaImpl implements ServicePersona{

    @Inject
    private PersonaDao personaDao;
    
    
    @Override
    public List<Persona> listarPersonas() {
        return personaDao.listar();
    }

    
    @Override
    public void insertarPersona(Persona persona) {
        personaDao.insertarPersona(persona);
    }

    @Override
    public boolean validarLogin(int id, String pass) {
        return personaDao.validarLogin(id, pass);
    }
    
}
