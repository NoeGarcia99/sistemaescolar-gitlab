package sisescolar.servicio;

import java.util.List;
import javax.ejb.Local;
import sisescolar.modelos.Persona;

@Local
public interface ServicePersona {
    public List<Persona> listarPersonas();
    public void insertarPersona(Persona persona);
    public boolean validarLogin(int id, String pass);
}
