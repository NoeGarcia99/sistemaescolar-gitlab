package sisescolar.servicio;

import javax.ejb.Stateless;
import javax.inject.Inject;
import sisescolar.datos.ProfesorDao;
import sisescolar.modelos.Profesor;

@Stateless
public class ProfesorServicioImpl implements ProfesorService{
    
    @Inject
    private ProfesorDao profesorDao;

    @Override
    public void insertarProfesor(Profesor profesor) {
        profesorDao.insertarProfesor(profesor);
    }
}
