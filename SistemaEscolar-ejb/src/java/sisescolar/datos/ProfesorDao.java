package sisescolar.datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.ejb.Stateless;
import sisescolar.modelos.Profesor;

@Stateless
public class ProfesorDao {
    public static String SQL_INSERT = "INSERT INTO profesor(id_persona, cedula, grado_academico, especialidad) "
            + "VALUES(?,?,?,?)";
    
    public int insertarProfesor(Profesor profesor){
    
        Conexion conn = new Conexion();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        int rows = 0;
        try {
            stmt = conn.getConnection().prepareStatement("SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1");
            rs = stmt.executeQuery();
            int idPersona = 0;
            while(rs.next()){
            idPersona = rs.getInt("id_persona");
            }
            
            stmt.clearBatch();
            stmt = conn.getConnection().prepareStatement(SQL_INSERT);
            stmt.setInt(1, idPersona);
            stmt.setString(2, profesor.getCedula());
            stmt.setString(3, profesor.getGrado_academico());
            stmt.setString(4, profesor.getEspecialidad());
            rows = stmt.executeUpdate();
            
            //stmt.executeQuery();
            stmt.close();  
        
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
            
        }
        
        
        
        return rows;
    }
}
