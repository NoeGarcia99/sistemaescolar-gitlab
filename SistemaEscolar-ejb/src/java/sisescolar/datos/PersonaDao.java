package sisescolar.datos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import sisescolar.modelos.Persona;

@Stateless
public class PersonaDao {
  
    public static final String SQL_SELECT = "SELECT id_persona, nombre, a_paterno, a_materno, sexo, telefono, email, passw, rol" + " FROM persona";
    public static final String SQL_SELECT_PASS_ID = "SELECT id_persona, passw" + " FROM persona WHERE (passw LIKE ?) AND (id_persona = ?)" ;
    public static final String SQL_INSERT = "INSERT INTO persona(nombre, a_paterno, a_materno, sexo, telefono, email, passw, rol) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    
    public static final String SQL_UPDATE = "UPDATE persona "
            +" SET nombre=?, a_paterno=?, a_materno=?, sexo=?, telefono=?, email=?, passw=? WHERE id_persona = ?";
    public static final String SQL_DELETE = "DELETE FROM persona WHERE id_persona=?";
    
    
    public List<Persona> listar(){
    
        Conexion conn = new Conexion();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Persona persona = null;
        List<Persona> personas = new ArrayList<>();
        
        try {
            stmt = conn.getConnection().prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                
                int idPersona = rs.getInt("id_persona");
                String nombre = rs.getString("nombre");
                String aPaterno = rs.getString("a_paterno");
                String aMaterno = rs.getString("a_materno");
                String sexo = rs.getString("sexo");
                String telefono = rs.getString("telefono");
                String email = rs.getString("email");    
                String passw = rs.getString("passw");
                String rol = rs.getString("rol");
                persona = new Persona(idPersona,nombre, aPaterno, aMaterno, sexo, telefono, email, passw, rol);
                personas.add(persona);
            }
            
            rs.close();
            stmt.close();  
        
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
            
        }
        
        return personas;
        
    }
    
    public int insertarPersona(Persona persona){
        
        Conexion conn = new Conexion();
        PreparedStatement stmt = null;
        
        int rows = 0;
        try {
            stmt = conn.getConnection().prepareStatement(SQL_INSERT);
            stmt.setString(1, persona.getNombre());
            stmt.setString(2, persona.getPaterno());
            stmt.setString(3, persona.getMaterno());
            stmt.setString(4, persona.getSexo());
            stmt.setString(5, persona.getTelefono());
            stmt.setString(6, persona.getEmail());
            stmt.setString(7, persona.getPassw());
            stmt.setString(8, persona.getRol());
            
            rows = stmt.executeUpdate();
            
            //stmt.executeQuery();
            stmt.close();  
        
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
            
        }
        
        
        
        return rows;
    }
    
    
    public boolean validarLogin(int id, String pass){
        
        Conexion conn = new Conexion();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = conn.getConnection().prepareStatement(SQL_SELECT_PASS_ID);
            stmt.setString(1, pass);
            stmt.setInt(2, id);
            rs = stmt.executeQuery();
            
             while (rs.next()){
                 return true;
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        
        return false;
        
    }
   
}
