
package sisescolar.datos;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.ejb.Stateless;
import sisescolar.modelos.Alumno;

@Stateless
public class AlumnoDao {
    public static String SQL_INSERT = "INSERT INTO alumno(id_persona, clave) "
            + "VALUES(?,?)";
    
    public int InsertarAlumno(Alumno alumno) {
        
        Conexion conn = new Conexion();
        PreparedStatement stmt = null;
        int rows = 0;
        try {
            stmt = conn.getConnection().prepareStatement(SQL_INSERT);
            stmt.setInt(1, alumno.getIdPersona());
            stmt.setString(2, alumno.getClave());
            
            rows = stmt.executeUpdate();
            stmt.close();
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        
        return rows;
    }
}
