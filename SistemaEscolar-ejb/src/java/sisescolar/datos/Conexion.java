package sisescolar.datos;


import java.sql.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Conexion {
    
   private Connection cp;
   
   public Conexion(){
       try {
           InitialContext ic = new InitialContext();
           //en esta parte es donde ponemos el Nombre
           //de JNDI para que traiga el datasource
           DataSource ds = (DataSource) ic.lookup("jdbc/postgres");
           cp = ds.getConnection();
       } catch (NamingException ex) {
           ex.printStackTrace();
       } catch (SQLException ex) {
            ex.printStackTrace();
       }
       
   }
   
   
   public Connection getConnection(){
       return cp;
   }
   
    public void close(){
    
        try {
            cp.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    
    }
    
}
